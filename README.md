## Marvel Example


Toplam harcanılan efor 5 saatdir.

## Feature

● Anasayfada karakterler listelenmeli. (karakterin adı ve fotoğrafı)

● İlk açılışta 30 ve scroll ile sayfa sonuna ulaşıldığında sonraki sayfaya içeriği gelmeli,

● Her sonraki sayfada 30 adet olacak şekilde ilerlemeli

● Karakterlerin üzerine tıklandığında ise detay sayfasına gitmeli ve bu sayfada karakterin ( adı, fotoğrafı, açıklaması (description), ve yer aldığı çizgi romanları (comics) ) listelenmeli.

● Detay sayfasında listelenen karakterin yer aldığı çizgi romanlar 2005 yılından sonra yayınlanmış olmalı ve yayınlanma tarihi yeniden eskiye doğru sıralanmalı. Son 10 çizgi roman ile limitlendirilmeli.

● Uygulama geliştirilirken MVVMC design pattern kullanılması gerekmektedir.

Screenshots
----------

|  (/images/1.png)	                      | <img src="/images/2.png">       |   <img src="/images/3.png"> 	  | <img src="/images/4.png">       |  <img src="/images/5.png">
| :-------------------------------------: | :-------------------------------------: | :-------------------------------------: | :-------------------------------------: |:-------------------------------------: |
|                Safari problem           |                Analytics                |                 Crashlytic              |                Analytics                |     Marvel Api internal server error


Installing
----------
https://Omererdogan@bitbucket.org/Omererdogan/marvelexample.git

extension paketini MarvelExample.xcworkspace olduğu klasöre clone layalım.
https://Omererdogan@bitbucket.org/Omererdogan/marvelextensions.git


Feature
----------
● Local storage ile favori karakter sayfası 

● UI/UX ve Animasyon

● RxSwift

● Analytics integration


Postman Collection & Globals
----------
collection
https://bitbucket.org/Omererdogan/marvelexample/raw/83c8c30dc2aff43e0a2fe3abd03fabf071954a6b/marvelexample/images/Marvel.postman_collection.json

globals
https://bitbucket.org/Omererdogan/marvelexample/raw/83c8c30dc2aff43e0a2fe3abd03fabf071954a6b/marvelexample/images/My%20Workspace.postman_globals.json

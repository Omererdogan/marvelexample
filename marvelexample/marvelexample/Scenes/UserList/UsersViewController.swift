//
//  UsersViewController.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 21.09.2021.
//

import RxCocoa
import RxSwift
import UIKit

class UsersViewController: UIViewController, BindableType {
    var viewModel: UsersViewModel!

    // MARK: Views

    private var tableView: UITableView!

    // MARK: Stored properties

    private let disposeBag = DisposeBag()
    private let cellIdentifier = String(describing: DetailTableViewCell.self)

    // MARK: Initialization

    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        configureTableViewCell()
        configureNavigationBar()
    }

    // MARK: BindableType

    func bindViewModel() {
        viewModel.output.users
        .bind(to: tableView.rx.items(cellIdentifier: cellIdentifier)) { _, element, cell in
            if let cellToUse = cell as? DetailTableViewCell {
                cellToUse.titleLabel.text = element.title
                cellToUse.textLabel?.text = element.name
                cellToUse.selectionStyle = .none
            }
        
        }
        .disposed(by: disposeBag)

        tableView.rx.modelSelected(User.self)
            .bind(to: viewModel.input.showUserTrigger)
            .disposed(by: disposeBag)
    }
    func setViews(){
        tableView = UITableView()
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(10)
            make.bottom.left.right.equalToSuperview()
        }
       
    }
    // MARK: Helpers

    private func configureTableViewCell() {
        tableView.register(DetailTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.backgroundColor = .lightGray
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .singleLine
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
    }

    private func configureNavigationBar() {
        title = "Users"
    }

}

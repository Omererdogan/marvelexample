//
//  UsersViewModel.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 21.09.2021.
//

import Action
import RxSwift
import Coordinator

protocol UsersViewModelInput {
    var showUserTrigger: AnyObserver<User> { get }
}

protocol UsersViewModelOutput {
    var users: Observable<[User]> { get }
}

protocol UsersViewModel {
    var input: UsersViewModelInput { get }
    var output: UsersViewModelOutput { get }
}

extension UsersViewModel where Self: UsersViewModelInput & UsersViewModelOutput {
    var input: UsersViewModelInput { return self }
    var output: UsersViewModelOutput { return self }
}

//
//  UsersViewModelAction.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 21.09.2021.
//

import Action
import RxSwift
import Coordinator
import CoordinatorRx

class UsersViewModelAction: UsersViewModel, UsersViewModelInput, UsersViewModelOutput {

    // MARK: Inputs

    private(set) lazy var showUserTrigger = showUserAction.inputs

    // MARK: Actions

    private lazy var showUserAction = Action<User, Void> { [unowned self] user in
        self.router.rxx.trigger(.user(user.name))
    }

    // MARK: Outputs

    private(set) lazy var users = Observable.just(userService.allUsers())

    // MARK: Stored properties

    private let userService: UserService
    private let router: UnownedRouter<UserListRoute>

    // MARK: Initialization

    init(userService: UserService, router: UnownedRouter<UserListRoute>) {
        self.userService = userService
        self.router = router
    }

}

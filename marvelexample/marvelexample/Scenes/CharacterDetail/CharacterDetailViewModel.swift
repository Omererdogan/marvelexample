//
//  CharacterDetailViewModel.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Action
import RxSwift

protocol CharacterDetailViewModelInput {
}

protocol CharacterDetailViewModelOutput {
    var characters: Observable<MarvelResult> { get }
}

protocol CharacterDetailViewModel {
    var input: CharacterDetailViewModelInput { get }
    var output: CharacterDetailViewModelOutput { get }
}

extension CharacterDetailViewModel where Self: CharacterDetailViewModelInput & CharacterDetailViewModelOutput {
    var input: CharacterDetailViewModelInput { return self }
    var output: CharacterDetailViewModelOutput { return self }
}

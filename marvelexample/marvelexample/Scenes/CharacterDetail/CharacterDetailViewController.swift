//
//  CharacterDetailViewController.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import RxCocoa
import RxSwift
import UIKit
import Utils

class CharacterDetailViewController: UIViewController, BindableType {
    
    var viewModel: CharacterDetailViewModel!

    // MARK: Views

    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var contentTextView: UITextView!
    // MARK: Stored properties
    
    private let disposeBag = DisposeBag()

    // MARK: Overrides

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        contentTextView.setContentOffset(.zero, animated: false)
    }

    // MARK: BindableType

    func bindViewModel() {
        viewModel.output.characters
            .map { $0.name }
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)

        viewModel.output.characters
            .map { $0.resultDescription }
            .bind(to: contentTextView.rx.text)
            .disposed(by: disposeBag)

        viewModel.output.characters
            .map {downloadImageForUrl(url: $0.resourceURI).bind(to: self.imageView.rx.image).disposed(by: self.disposeBag)}
        
       
    }
}

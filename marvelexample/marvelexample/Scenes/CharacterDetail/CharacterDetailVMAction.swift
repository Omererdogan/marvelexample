//
//  CharacterDetailVMAction.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Action
import RxSwift
import Coordinator


class CharacterDetailVMAction: CharacterDetailViewModel, CharacterDetailViewModelInput, CharacterDetailViewModelOutput {
    
    // MARK: Outputs

    let characters: Observable<MarvelResult>

    // MARK: Initialization

    init(characters: MarvelResult) {
        self.characters = .just(characters)
    }

}

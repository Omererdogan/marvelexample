//
//  DetailTableViewCell.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import UIKit

class DetailTableViewCell: UITableViewCell {

    private let containerForShadow = UIView()
    public let titleLabel = UILabel()
    
    // MARK: Initialization

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        setViews()
        configureViews()

    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setViews(){
        contentView.addSubview(containerForShadow)
        containerForShadow.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.top.equalTo(10)
            make.bottom.equalTo(-10)
        }
        containerForShadow.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(10)
            make.right.equalTo(-10)
            make.top.equalTo(10)
            make.bottom.equalTo(-10)
        }
        containerForShadow.backgroundColor = .lightText
        contentView.backgroundColor = .white
        
        containerForShadow.layer.cornerRadius = 20
    }
    
    func configureViews(){
        titleLabel.numberOfLines = 3
        titleLabel.textColor = .red
        titleLabel.lineBreakMode = .byWordWrapping
    }

    // MARK: Overrides
    
    public override func layoutSubviews() {
        containerForShadow.layer.shadowColor = UIColor.black.cgColor
        containerForShadow.layer.shadowOffset = CGSize(width: 1, height: 5)
        containerForShadow.layer.shadowRadius = 5
        containerForShadow.layer.shadowOpacity = 0.17
    }
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)

        UIView.animate(withDuration: animated ? 0.35 : 0) {
            
            
        }
    }

}

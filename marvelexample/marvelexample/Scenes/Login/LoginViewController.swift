//
//  LoginViewController.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import RxCocoa
import RxSwift
import UIKit
import Lottie
import SnapKit

class LoginViewController: UIViewController, BindableType {
    
    let animationView = AnimationView()
    
    var viewModel: LoginViewModel!

    // MARK: Stored properties
    
    private let disposeBag = DisposeBag()
    
    // MARK: Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setanimation()
        setViews()
        
    }
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = .init(rgb: 0xEF151E)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }
    
    // MARK: Views
    func setViews(){
        view.addSubview(animationView)
        animationView.snp.makeConstraints { make in
            make.top.bottom.left.right.equalToSuperview()
        }
        view.bringSubviewToFront(animationView)
    }
    
    func setanimation() {
        let animation = Animation.named("marvelAnim", subdirectory: "TestAnimations")
        
        animationView.animation = animation
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .repeat(1)
        animationView.animationSpeed = 4.3
        animationView.play { isFinished in
            self.viewModel.input.loginTrigger.onNext(())
        }
    }
    // MARK: BindableType
    
    func bindViewModel() {
   
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
}

extension LoginViewController {
    @objc func updateAnimation(sender: UISlider) {
        animationView.currentProgress = CGFloat(sender.value)
    }
    
    @objc func animationCallback() {
        if animationView.isAnimationPlaying {
            print("lottie!!!!!")
        }
    }
}

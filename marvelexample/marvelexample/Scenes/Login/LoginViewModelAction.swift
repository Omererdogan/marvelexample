//
//  LoginViewModelAction.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Action
import RxSwift
import Coordinator
import CoordinatorRx

class LoginViewModelAction: LoginViewModel, LoginViewModelInput, LoginViewModelOutput {

    // MARK: Inputs

    private(set) lazy var loginTrigger = loginAction.inputs

    // MARK: Actions

    private lazy var loginAction = CocoaAction { [unowned self] in
        self.router.rxx.trigger(.home(nil))
    }

    // MARK: Stored properties

    private let router: UnownedRouter<AppRoute>

    // MARK: Initialization

    init(router: UnownedRouter<AppRoute>) {
        self.router = router
    }
}

//
//  LoginViewModel.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Action
import RxSwift
import Coordinator

protocol LoginViewModelInput {
    var loginTrigger: AnyObserver<Void> { get }
}

protocol LoginViewModelOutput {}

protocol LoginViewModel {
    var input: LoginViewModelInput { get }
    var output: LoginViewModelOutput { get }
}

extension LoginViewModel where Self: LoginViewModelInput & LoginViewModelOutput {
    var input: LoginViewModelInput { return self }
    var output: LoginViewModelOutput { return self }
}

//
//  AboutViewModelAction.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Foundation
import RxSwift
import Action
import Coordinator
import CoordinatorRx

class AboutViewModelAction: AboutViewModel, AboutViewModelInput, AboutViewModelOutput {

    // MARK: Inputs

    private(set) lazy var openWebsiteTrigger = openWebsiteAction.inputs

    // MARK: Actions

    private lazy var openWebsiteAction = CocoaAction { [unowned self] in
        self.router.rxx.trigger(.website)
    }

    // MARK: Outputs

    let url = URL(string: "https://www.marvel.com/corporate/about")!

    // MARK: Stored properties

    private let router: UnownedRouter<AboutRoute>

    // MARK: Initialization

    init(router: UnownedRouter<AboutRoute>) {
        self.router = router
    }

}

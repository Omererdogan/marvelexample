//
//  AboutViewController.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import UIKit
import RxSwift
import RxCocoa
import WebKit
import SnapKit

class AboutViewController: UIViewController, BindableType {

    var viewModel: AboutViewModel!

    // MARK: Views

    private let webView: WKWebView = {
        let view = WKWebView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    // MARK: Stored properties

    private let disposeBag = DisposeBag()

    // MARK: Overrides

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Marvel About"
        setupWebView()
    }

    // MARK: BindableType

    func bindViewModel() {
        let request = URLRequest(url: viewModel.output.url)
        webView.load(request)
    }

    // MARK: Actions

    @objc
    private func openWebsite() {
        viewModel.input.openWebsiteTrigger.onNext(())
    }

    // MARK: Helpers

    private func setupWebView() {
        view.addSubview(webView)

        webView.snp.makeConstraints { make in
            make.bottom.top.left.right.equalTo(view.safeAreaLayoutGuide)
        }
    }

}

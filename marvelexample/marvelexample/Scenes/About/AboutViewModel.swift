//
//  AboutViewModel.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Foundation
import RxSwift
import Action

protocol AboutViewModelInput {
    var openWebsiteTrigger: AnyObserver<Void> { get }
}

protocol AboutViewModelOutput {
    var url: URL { get }
}

protocol AboutViewModel {
    var input: AboutViewModelInput { get }
    var output: AboutViewModelOutput { get }
}

extension AboutViewModel where Self: AboutViewModelInput & AboutViewModelOutput {
    var input: AboutViewModelInput { return self }
    var output: AboutViewModelOutput { return self }
}

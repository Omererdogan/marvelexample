//
//  CharactersViewModel.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Action
import RxSwift

protocol CharactersViewModelInput {
    var selectedCharacters: AnyObserver<MarvelResult> { get }
}

protocol CharactersViewModelOutput {
    var characters: Observable<[MarvelResult]> { get }
    var title: Observable<String> { get }
}

protocol CharactersViewModel {
    var input: CharactersViewModelInput { get }
    var output: CharactersViewModelOutput { get }
}

extension CharactersViewModel where Self: CharactersViewModelInput & CharactersViewModelOutput {
    var input: CharactersViewModelInput { return self }
    var output: CharactersViewModelOutput { return self }
}

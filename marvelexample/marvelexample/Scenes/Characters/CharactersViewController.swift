//
//  CharactersViewController.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import RxCocoa
import RxSwift
import UIKit
import Utils

class CharactersViewController: UIViewController, BindableType {
    
    var viewModel: CharactersViewModel!

    // MARK: Views

    private var tableView: UITableView!

    // MARK: Stored properties
    
    private let disposeBag = DisposeBag()
    private let tableViewCellIdentifier = String(describing: DetailTableViewCell.self)


    // MARK: Overrides

    override func viewDidLoad() {
        super.viewDidLoad()

        setViews()
        title = "Characters"
        tableView.register(DetailTableViewCell.self, forCellReuseIdentifier: tableViewCellIdentifier)
    }
    
    func setViews(){
        tableView = UITableView()
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(10)
            make.bottom.left.right.equalToSuperview()
        }
        
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .singleLine
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
    }

    // MARK: BindableType

    func bindViewModel() {
        viewModel.output.characters
            .bind(to: tableView.rx.items(cellIdentifier: tableViewCellIdentifier)) { _, model, cell in
                cell.textLabel?.text = model.name
                cell.detailTextLabel?.text = model.resultDescription
                downloadImageForUrl(url: model.resourceURI).subscribe(onNext: { (data) in
                    cell.imageView?.image = data
                }).disposed(by: self.disposeBag)
                cell.selectionStyle = .none
            }
            .disposed(by: disposeBag)

        tableView.rx.modelSelected(MarvelResult.self)
            .bind(to: viewModel.input.selectedCharacters)
            .disposed(by: disposeBag)

        viewModel.output.title
            .bind(to: navigationItem.rx.title)
            .disposed(by: disposeBag)
    }

}


//
//  CharactersVMAction.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Action
import RxSwift
import Coordinator
import CoordinatorRx

class CharactersVMAction: CharactersViewModel, CharactersViewModelInput, CharactersViewModelOutput {

    // MARK: Inputs

    private(set) lazy var selectedCharacters = charactersSelectedAction.inputs

    // MARK: Actions

    lazy var charactersSelectedAction = Action<MarvelResult, Void> { [unowned self] characters in
        self.router.rxx.trigger(.charactersDetail(characters))
    }

    // MARK: Outputs

    private(set) lazy var characters = charactersObservable.map { $0.articles }
    private(set) lazy var title = charactersObservable.map { $0.title }

    let charactersObservable: Observable<(title: String, articles: [MarvelResult])>

    // MARK: Stored properties

    private let charactersService: CharactersService
    private let router: UnownedRouter<CharactersRoute>

    // MARK: Initialization

    init(charactersService: CharactersService, router: UnownedRouter<CharactersRoute>) {
        self.charactersService = charactersService
        self.charactersObservable = .just(charactersService.mostRecentCharacters())
        self.router = router
    }

}

//
//  UserViewModelAction.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 21.09.2021.
//

import Action
import RxSwift
import Coordinator
import CoordinatorRx

class UserViewModelAction: UserViewModel, UserViewModelInput, UserViewModelOutput {

    // MARK: Inputs

    private(set) lazy var alertTrigger = alertAction.inputs
    private(set) lazy var closeTrigger = closeAction.inputs

    // MARK: Actions

    private lazy var alertAction = CocoaAction { [unowned self] in
        self.router.rxx.trigger(.alert(title: "Hey", message: "You are awesome!"))
    }

    private lazy var closeAction = CocoaAction { [unowned self] in
        self.router.rxx.trigger(.users)
    }

    // MARK: Outputs

    let username: Observable<String>

    // MARK: Stored properties

    private let router: UnownedRouter<UserRoute>

    // MARK: Initialization

    init(router: UnownedRouter<UserRoute>, username: String) {
        self.router = router
        self.username = .just(username)
    }
}

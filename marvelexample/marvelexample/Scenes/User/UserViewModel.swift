//
//  UserViewModel.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 21.09.2021.
//

import Action
import RxSwift
import Coordinator

protocol UserViewModelInput {
    var alertTrigger: AnyObserver<Void> { get }
    var closeTrigger: AnyObserver<Void> { get }
}

protocol UserViewModelOutput {
    var username: Observable<String> { get }
}

protocol UserViewModel {
    var input: UserViewModelInput { get }
    var output: UserViewModelOutput { get }
}

extension UserViewModel where Self: UserViewModelInput & UserViewModelOutput {
    var input: UserViewModelInput { return self }
    var output: UserViewModelOutput { return self }
}

//
//  HomeViewModelAction.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Action
import RxSwift
import Coordinator
import CoordinatorRx

class HomeViewModelAction: HomeViewModel, HomeViewModelInput, HomeViewModelOutput {

    
    // MARK: Inputs

    private(set) lazy var logoutTrigger = logoutAction.inputs
    private(set) lazy var aboutTrigger = aboutAction.inputs

    // MARK: Actions

    private lazy var logoutAction = CocoaAction { [unowned self] in
        self.router.rxx.trigger(.logout)
    }

    private lazy var aboutAction = CocoaAction { [unowned self] in
        self.router.rxx.trigger(.about)
    }
    // MARK: Stored properties

    private let router: UnownedRouter<UserListRoute>

    // MARK: Initialization

    init(router: UnownedRouter<UserListRoute>) {
        self.router = router
    }

    // MARK: Methods

    func registerPeek(for sourceView: Container) {
        router.trigger(.registerUsersPeek(from: sourceView))
    }

}

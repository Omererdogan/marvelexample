//
//  HomeViewModel.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Action
import RxSwift
import Coordinator

protocol HomeViewModelInput {
    var logoutTrigger: AnyObserver<Void> { get }
    var aboutTrigger: AnyObserver<Void> { get }
}

protocol HomeViewModelOutput {}

protocol HomeViewModel {
    var input: HomeViewModelInput { get }
    var output: HomeViewModelOutput { get }

    func registerPeek(for sourceView: Container)
}

extension HomeViewModel where Self: HomeViewModelInput & HomeViewModelOutput {
    var input: HomeViewModelInput { return self }
    var output: HomeViewModelOutput { return self }
}

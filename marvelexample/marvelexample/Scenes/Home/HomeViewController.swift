//
//  HomeViewController.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import RxCocoa
import RxSwift
import UIKit

class HomeViewController: UIViewController, BindableType {
    var viewModel: HomeViewModel!

    // MARK: Views

    @IBOutlet private var logoutButton: UIButton!
    @IBOutlet private var usersButton: UIButton!
    @IBOutlet private var aboutButton: UIButton!
    
    // MARK: Stored properties

    private let disposeBag = DisposeBag()

    // MARK: Overrides

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Home"
    }
    
    override func loadView() {

        view = UIView()
        view.backgroundColor = .init(rgb: 0xEF151E)
    }

    // MARK: BindableType

    func bindViewModel() {
//        logoutButton.rx.tap
//            .bind(to: viewModel.input.logoutTrigger)
//            .disposed(by: disposeBag)
//
//        aboutButton.rx.tap
//            .bind(to: viewModel.input.aboutTrigger)
//            .disposed(by: disposeBag)
//
//        viewModel.registerPeek(for: usersButton)
    }

}


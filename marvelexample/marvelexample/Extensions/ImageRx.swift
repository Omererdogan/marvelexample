//
//  ImageRx.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 21.09.2021.
//

import RxAlamofire
import UIKit
import RxSwift

public func downloadImageForUrl(url: String) -> Observable<UIImage?> {
    return RxAlamofire
        .requestData(.get, url)
        .map({ (response,data) -> UIImage? in
            return UIImage(data: data)
        })
}

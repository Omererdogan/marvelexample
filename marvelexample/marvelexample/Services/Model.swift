//
//  Model.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 22.09.2021.
//

import Foundation
import Moya
import HandyJSON
import RxSwift
import RxMoya

final class Model {
    func request<H: HandyJSON, T: TargetType>(_ type: T, _ model: H.Type) -> Observable<H> {
        let mp = MoyaProvider<T>()
        let ob = mp
            .rx.request(type)
            .asObservable()
            .map(model)
        return ob
    }
}

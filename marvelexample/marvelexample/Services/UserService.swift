//
//  UserService.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 21.09.2021.
//

protocol UserService {
    func allUsers() -> [User]
}

class MockUserService: UserService {
    func allUsers() -> [User] {
        let names: [String:String] = [
            "Stefan": "Malte", "Sebi":
                "Niko", "Balazs": "Patrick",
            "Julian": "Quirin", "Paul":
                "Michael", "Eduardo": "Lizzie"
        ]
        return names.map(User.init)
    }
}

//
//  CharactersService.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import UIKit
import RxSwift
import Alamofire
import ReactiveMoya
import Moya
import HandyJSON

// swiftlint:disable line_length

protocol CharactersServiceDelegate {
    func mostRecentCharacters() -> (title: String, articles: [MarvelResult])
}

class CharactersService: CharactersServiceDelegate {

//    private let userService = MockUserService()
    private let disposeBag = DisposeBag()
    var data: [MarvelResult] = []
    let model: Model = Model()


    private let images = [UIColor.black, .blue, .green, .yellow, .orange, .red, .white]
        .map { UIImage.color($0, size: CGSize(width: 44, height: 44))! }

    func mostRecentCharacters() -> (title: String, articles: [MarvelResult]) {
       
        return (title: "QuickBird Studios Blog", articles: [MarvelResult]())
    }
    
    func getCharacters() throws -> Observable<[MarvelResult]> {
        self.model.request(ApiService.characters, MarvelResult.self).mapResponseToObjectArray(type: MarvelResult.self)
            .do(onNext: {
                self.data = $0
            })
    }

}

extension UIImage {

    static func color(_ color: UIColor, size: CGSize = .init(width: 1, height: 1)) -> UIImage! {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContext(rect.size)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        defer { UIGraphicsEndImageContext() }
        context.setFillColor(color.cgColor)
        context.fill(rect)
        return UIGraphicsGetImageFromCurrentImageContext()
    }

}

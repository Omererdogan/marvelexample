//
//  ModalProtocol.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 22.09.2021.
//

import Foundation
import HandyJSON
import Moya
import RxSwift

protocol ModelAbstract {
    associatedtype JsonFormat: HandyJSON
    associatedtype APIFormat: TargetType
    func request(_ type: APIFormat, _ model: JsonFormat) -> Observable<JsonFormat>
}

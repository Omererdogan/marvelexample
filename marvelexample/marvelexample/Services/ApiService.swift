//
//  ApiService.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 21.09.2021.
//

import Foundation
import Moya
import CryptoSwift

enum ApiService {
    case characters
    case charactersId(characterId: Int)
}

extension ApiService: TargetType {
    var headers: [String : String]? {
        return ["Content-Type":"application/json; charset=utf-8"]
    }
    
    public var task: Task {
        return .requestPlain
    }
    var parameterEncoding: ParameterEncoding { return URLEncoding.default }
    public var baseURL: URL { return URL(string:  Config.apiURL)! }
    
    public var path: String {
        switch self {
        case .characters:
            return "/characters"
        case .charactersId(characterId: let characterId):
            return "/characters/" + "\(characterId)"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .characters:
            return .get
        case .charactersId(characterId: _):
            return .get
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .characters:
            let timestamp = "\(Date().timeIntervalSince1970)"
            return ["ts" : timestamp, "apiKey" : Config.publicKey, "hash": "\(timestamp)\(Config.publicKey)\(Config.privateKey)".md5()]
            
        case .charactersId(characterId: _):
            let timestamp = "\(Date().timeIntervalSince1970)"
            return ["ts" : timestamp, "apiKey" : Config.publicKey, "hash": "\(timestamp)\(Config.publicKey)\(Config.privateKey)".md5()]
        }
    }
    
    public var validate: Bool {
        return true
    }
    public var sampleData: Data {
        return "[{\"characters\": \"Iron Man\"}]".data(using: String.Encoding.utf8)!
    }
}

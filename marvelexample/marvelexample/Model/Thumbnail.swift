//
//  Thumbnail.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Foundation
import HandyJSON

// MARK: - Thumbnail
struct Thumbnail: HandyJSON {
    
    init() {
    }
    
    var path: String?
    var thumbnailExtension: String?
}

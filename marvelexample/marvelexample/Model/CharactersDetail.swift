//
//  CharactersDetail.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Foundation
import HandyJSON

// MARK: - CharacterDetail
struct CharacterDetail: HandyJSON {
    
    init() {
    }
    
    var code: Int?
    var status, copyright, attributionText, attributionHTML: String?
    var etag: String
    var data: DataClass?
}



//
//  DataClass.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Foundation
import HandyJSON

// MARK: - DataClass
struct DataClass: HandyJSON {
    init() {
    }
    
    var offset, limit, total, count: Int?
    var results: [MarvelResult]?
}

class GenericDataClass <T> {
    let offset = 0, limit = 0, total = 0, count: Int = 0
    var results : [T?] = []
}





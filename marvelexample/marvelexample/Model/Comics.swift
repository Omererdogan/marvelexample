//
//  Comics.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Foundation
import HandyJSON

// MARK: - Comics
struct Comics: HandyJSON {
    init() {
    }
    
    var available: Int?
    var collectionURI: String?
    var items: [ComicsItem]?
    var returned: Int?
}

// MARK: - ComicsItem
struct ComicsItem: HandyJSON {
    init() {
    }
    
    var resourceURI: String?
    var name: String?
}

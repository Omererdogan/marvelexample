//
//  URLElement.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Foundation
import HandyJSON

// MARK: - URLElement
struct URLElement: HandyJSON {
    init() {
    }
    
    var type: String?
    var url: String?
}

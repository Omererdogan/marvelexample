//
//  User.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 21.09.2021.
//

import Foundation
import HandyJSON

struct User: HandyJSON {
    init() {
    }
    
    var name: String?
    var title: String?
}

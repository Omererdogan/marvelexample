//
//  Stories.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Foundation
import HandyJSON

// MARK: - Stories
struct Stories: HandyJSON {
    init() {
    }
    
    var available: Int?
    var collectionURI: String?
    var items: [StoriesItem]?
    var returned: Int?
}

// MARK: - StoriesItem
struct StoriesItem: HandyJSON {
    init() {
    }
    
    var resourceURI: String?
    var name, type: String?
}

//
//  HomePageCoordinator.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Coordinator

class HomePageCoordinator: PageCoordinator<HomeRoute> {

    // MARK: Stored properties

    private let charactersRouter: StrongRouter<CharactersRoute>


    // MARK: Initialization

    init(charactersRouter: StrongRouter<CharactersRoute> = CharactersCoordinator().strongRouter) {
        self.charactersRouter = charactersRouter
        
        super.init(
            rootViewController: .init(transitionStyle: .scroll,
                                      navigationOrientation: .horizontal,
                                      options: nil),
            pages: [charactersRouter], loop: false,
            set: charactersRouter, direction: .forward
        )
    }

    // MARK: Overrides

    override func prepareTransition(for route: HomeRoute) -> PageTransition {
        switch route {
        case .characters:
            return .set(charactersRouter, direction: .forward)

        }
    }

}

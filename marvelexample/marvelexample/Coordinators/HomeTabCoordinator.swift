//
//  HomeTabCoordinator.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import UIKit
import Coordinator

enum HomeRoute: Route {
    case characters
}

class HomeTabCoordinator: TabBarCoordinator<HomeRoute> {

    // MARK: Stored properties

    private let charactersRouter: StrongRouter<CharactersRoute>
    private let userListRouter: StrongRouter<UserListRoute>

    // MARK: Initialization

    convenience init() {
        let charactersCoordinator = CharactersCoordinator()

        charactersCoordinator.rootViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .mostViewed, tag: 0)
        let userListCoordinator = UserListCoordinator()
        let aboutImage = UIImageView(image: UIImage(named: "about"))
        aboutImage.contentMode = .scaleAspectFit
        userListCoordinator.rootViewController.tabBarItem = UITabBarItem(title: "About", image: aboutImage.image, tag: 1)
        

        self.init(charactersRouter: charactersCoordinator.strongRouter,
                  userListRouter: userListCoordinator.strongRouter)
    }

    init(charactersRouter: StrongRouter<CharactersRoute>, userListRouter: StrongRouter<UserListRoute>) {
        self.charactersRouter = charactersRouter
        self.userListRouter = userListRouter

        super.init(tabs: [charactersRouter, userListRouter], select: charactersRouter)
    }

    // MARK: Overrides

    override func prepareTransition(for route: HomeRoute) -> TabBarTransition {
        switch route {
        case .characters:
            return .select(charactersRouter)
        }
    }

}

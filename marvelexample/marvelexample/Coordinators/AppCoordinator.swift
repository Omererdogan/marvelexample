//
//  AppCoordinator.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import UIKit
import Coordinator
import Utils
import Animations

enum AppRoute: Route {
    case login
    case home(StrongRouter<HomeRoute>?)
    case charactersDetail(MarvelResult)

}

class AppCoordinator: NavigationCoordinator<AppRoute> {

    // MARK: Initialization

    init() {
        super.init(initialRoute: .login)
    }

    // MARK: Overrides

    override func prepareTransition(for route: AppRoute) -> NavigationTransition {
        switch route {
        case .login:
            let viewController = LoginViewController.instantiateFromNib()
            viewController.navigationController?.toolbar.isHidden = true
            let viewModel = LoginViewModelAction(router: unownedRouter)
            viewController.bind(to: viewModel)
            return .push(viewController)
        case let .home(router):
            if let router = router {
                return .presentFullScreen(router, animation: .fade)
            }

            self.trigger(.home(HomeTabCoordinator().strongRouter))
            return .pop(animation: .swirl)
        case .charactersDetail(let characters):
            return .multiple(
                .dismissAll(),
                .popToRoot(),
                deepLink(AppRoute.home(HomePageCoordinator().strongRouter),
                         HomeRoute.characters,
                         CharactersRoute.charactersDetail(characters))
            )
        }
    }

    // MARK: Methods


}


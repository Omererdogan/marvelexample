//
//  HomeSplitCoordinator.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Coordinator

class HomeSplitCoordinator: SplitCoordinator<HomeRoute> {

    // MARK: Stored properties

    private let charactersRouter: StrongRouter<CharactersRoute>

    // MARK: Initialization

    init(charactersRouter: StrongRouter<CharactersRoute> = CharactersCoordinator().strongRouter) {
        self.charactersRouter = charactersRouter
        super.init(master: charactersRouter, detail: charactersRouter)
    }

    // MARK: Overrides

    override func prepareTransition(for route: HomeRoute) -> SplitTransition {
        switch route {
        case .characters:
            return .showDetail(charactersRouter)
        }
    }

}

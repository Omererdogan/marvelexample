//
//  UserListCoordinator.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 21.09.2021.
//

import Coordinator

enum UserListRoute: Route {
    case home
    case users
    case user(String)
    case registerUsersPeek(from: Container)
    case logout
    case about
}

class UserListCoordinator: NavigationCoordinator<UserListRoute> {

    // MARK: Initialization
    
    init() {
        super.init(initialRoute: .about)
    }

    // MARK: Overrides

    override func prepareTransition(for route: UserListRoute) -> NavigationTransition {
        switch route {
        case .home:
            let viewController = HomeViewController()
            let viewModel = HomeViewModelAction(router: unownedRouter)
            viewController.bind(to: viewModel)
            return .push(viewController)
        case .users:
            let viewController = UsersViewController()
            let viewModel = UsersViewModelAction(userService: MockUserService(), router: unownedRouter)
            viewController.bind(to: viewModel)
            return .push(viewController, animation: .fade)
        case .user(let username):
            let coordinator = UserCoordinator(user: username)
            return .present(coordinator, animation: .default)
        case .registerUsersPeek(let source):
            if #available(iOS 13.0, *) {
                return .none()
            } else {
                return registerPeek(for: source, route: .users)
            }
        case .logout:
            return .dismiss()
        case .about:
            addChild(AboutCoordinator(rootViewController: rootViewController))
            return .none()
        }
    }

}

//
//  AboutCoordinator.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import UIKit
import Coordinator

enum AboutRoute: Route {
    case home
    case website
}

class AboutCoordinator: NavigationCoordinator<AboutRoute> {
    
    // MARK: Initialization
    
    init(rootViewController: UINavigationController) {
        super.init(rootViewController: rootViewController, initialRoute: nil)
        trigger(.home)
    }

    // MARK: Overrides
    
    override func prepareTransition(for route: AboutRoute) -> NavigationTransition {
        switch route {
        case .home:
            let viewController = AboutViewController()
            let viewModel = AboutViewModelAction(router: unownedRouter)
            viewController.bind(to: viewModel)
            return .push(viewController)
        case .website:
            let url = URL(string: "https://www.marvel.com/corporate/about")!
            return Transition(wellCoordinators: [], animationInUse: nil) { _, _, completion in
                UIApplication.shared.open(url)
                completion?()
            }
        }
    }

    // MARK: Actions

    @objc
    private func openWebsite() {
        trigger(.website)
    }
    
}

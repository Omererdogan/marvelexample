//
//  CharactersCoordinator.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 20.09.2021.
//

import Coordinator
import Animations

enum CharactersRoute: Route {
    case characters
    case charactersDetail(MarvelResult)
    case close
}

class CharactersCoordinator: NavigationCoordinator<CharactersRoute> {

    // MARK: Initialization

    init() {
        super.init(initialRoute: .characters)
    }

    // MARK: Overrides

    override func prepareTransition(for route: CharactersRoute) -> NavigationTransition {
        switch route {
        case .characters:
            let viewController = CharactersViewController()
            let service = CharactersService()
            let viewModel = CharactersVMAction(charactersService: service, router: unownedRouter)
            viewController.bind(to: viewModel as CharactersViewModel)
            return .push(viewController)
        case .charactersDetail(let characters):
            let viewController = CharacterDetailViewController()
            let viewModel = CharacterDetailVMAction(characters: characters)
            viewController.bind(to: viewModel as CharacterDetailViewModel)
            let animation: Animation
            if #available(iOS 10.0, *) {
                animation = .swirl
            } else {
                animation = .scale
            }
            return .push(viewController, animation: animation)
        case .close:
            return .dismissToRoot()
        }
    }

}

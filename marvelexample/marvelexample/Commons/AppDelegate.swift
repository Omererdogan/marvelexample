//
//  AppDelegate.swift
//  marvelexample
//
//  Created by Ömer Erdoğan on 19.09.2021.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    private lazy var mainWindow = UIWindow()
    private let router = AppCoordinator().strongRouter

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configUI()
        FirebaseApp.configure()
        router.setRoot(for: mainWindow)
        return true
    }
    
    private func configUI() {
        UIView.appearance().overrideUserInterfaceStyle = .light
    }

}

